-- Created by Vertabelo (http://vertabelo.com)
-- Last modification date: 2022-04-05 10:04:16.117

-- tables
-- Table: Comments
CREATE TABLE Comments (
    comment_id serial  NOT NULL,
    comment_content text  NOT NULL,
    comment_date timestamp  NOT NULL,
    comment_employee_id int  NOT NULL,
    comment_prev_version int  NOT NULL,
    comment_next_version int  NULL,
    comment_clause text  NOT NULL,
    CONSTRAINT Comments_pk PRIMARY KEY (comment_id)
);

-- Table: Companies
CREATE TABLE Companies (
    company_id serial  NOT NULL,
    company_name text  NOT NULL,
    company_token text  NOT NULL UNIQUE,
    company_email text  NOT NULL UNIQUE,
    company_data json  NULL,
    company_inn varchar(15)  NOT NULL UNIQUE,
    CONSTRAINT Companies_pk PRIMARY KEY (company_id)
);

-- Table: Contracts
CREATE TABLE Contracts (
    contract_id serial  NOT NULL,
    contract_number text  NULL,
    contract_type text  NOT NULL,
    contract_status int  NOT NULL,
    contract_create_date timestamp  NOT NULL,
    contract_data json  NOT NULL,
    contract_creator_id int  NOT NULL,
    contract_responsible_id int  NOT NULL,
    CONSTRAINT Contracts_pk PRIMARY KEY (contract_id)
);

-- Table: Departaments
CREATE TABLE Departaments (
    departament_id serial  NOT NULL,
    departament_name text  NOT NULL,
    departament_company_id int  NOT NULL,
    CONSTRAINT Departaments_pk PRIMARY KEY (departament_id)
);

-- Table: Employers
CREATE TABLE Employers (
    employee_id serial  NOT NULL,
    employee_name text  NOT NULL,
    employee_email text  NOT NULL,
    employee_token text  NOT NULL,
    employee_departament_id int  NOT NULL,
    CONSTRAINT Employers_pk PRIMARY KEY (employee_id)
);

-- Table: Routes
CREATE TABLE Routes (
    route_id serial  NOT NULL,
    route_name text  NOT NULL,
    route_data json  NOT NULL,
    CONSTRAINT Routes_pk PRIMARY KEY (route_id)
);

-- Table: Versions
CREATE TABLE Versions (
    version_id serial  NOT NULL,
    version_contract_id int  NOT NULL,
    version_file oid  NOT NULL,
    version_creator int  NOT NULL,
    version_date timestamp  NOT NULL,
    version_routes_id int  NOT NULL,
    CONSTRAINT Versions_pk PRIMARY KEY (version_id)
);

-- foreign keys
-- Reference: Comments_Employers (table: Comments)
ALTER TABLE Comments ADD CONSTRAINT Comments_Employers
    FOREIGN KEY (comment_employee_id)
    REFERENCES Employers (employee_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Comments_document_version (table: Comments)
ALTER TABLE Comments ADD CONSTRAINT Comments_document_version
    FOREIGN KEY (comment_prev_version)
    REFERENCES Versions (version_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: ContractCreator_Employers (table: Contracts)
ALTER TABLE Contracts ADD CONSTRAINT ContractCreator_Employers
    FOREIGN KEY (contract_creator_id)
    REFERENCES Employers (employee_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: ContractResponsible_Employers (table: Contracts)
ALTER TABLE Contracts ADD CONSTRAINT ContractResponsible_Employers
    FOREIGN KEY (contract_responsible_id)
    REFERENCES Employers (employee_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Departament_Company (table: Departaments)
ALTER TABLE Departaments ADD CONSTRAINT Departament_Company
    FOREIGN KEY (departament_company_id)
    REFERENCES Companies (company_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Employers_Departament (table: Employers)
ALTER TABLE Employers ADD CONSTRAINT Employers_Departament
    FOREIGN KEY (employee_departament_id)
    REFERENCES Departaments (departament_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Versions_Comments (table: Versions)
ALTER TABLE Versions ADD CONSTRAINT Versions_Comments
    FOREIGN KEY (version_id)
    REFERENCES Comments (comment_next_version)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: Versions_Routes (table: Versions)
ALTER TABLE Versions ADD CONSTRAINT Versions_Routes
    FOREIGN KEY (version_routes_id)
    REFERENCES Routes (route_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: document_version_Contract (table: Versions)
ALTER TABLE Versions ADD CONSTRAINT document_version_Contract
    FOREIGN KEY (version_contract_id)
    REFERENCES Contracts (contract_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- Reference: document_version_Employers (table: Versions)
ALTER TABLE Versions ADD CONSTRAINT document_version_Employers
    FOREIGN KEY (version_creator)
    REFERENCES Employers (employee_id)  
    NOT DEFERRABLE 
    INITIALLY IMMEDIATE
;

-- End of file.

