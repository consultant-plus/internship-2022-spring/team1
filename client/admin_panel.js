let numberOfEmployees  = JSON.parse(localStorage.getItem("numberOfEmployees"));
let numberOfDeparts  = JSON.parse(localStorage.getItem("numberOfDeparts"));
let numberOfRoutes  = JSON.parse(localStorage.getItem("numberOfRoutes"));
let employees = JSON.parse(localStorage.getItem("employees"));
let departments = JSON.parse(localStorage.getItem("departments"));
let routes = JSON.parse(localStorage.getItem("routes"));
let userToken = JSON.parse(sessionStorage.getItem("userToken"));
localStorage.setItem("viewingDocId", -1);

let span = document.getElementsByClassName('navbar-text')[0];
span.innerHTML = employees.find(emps => emps.token === userToken).name + ', ' + 
                employees.find(emps => emps.token === userToken).department;

let tokens = employees.map(emps => emps.token);

drawTableOfEmployees('employeesTableBody');
drawTableOfRoutes('routesTableBody');
drawListOfEmployees('accordionEmployeesListBody');

function drawTableOfEmployees(tableBodyId) {
    let deleteButton = new Array(numberOfEmployees);
    let table = document.getElementById(tableBodyId);
    let rows = new Array(numberOfEmployees);
    let tds = new Array(5);
    for (let i = 0; i < numberOfEmployees; i++) {
        rows[i] = document.createElement("tr");
        deleteButton[i] = document.createElement('button');
        deleteButton[i].setAttribute("type", "button");
        deleteButton[i].setAttribute("class", "btn btn-danger");
        deleteButton[i].setAttribute("onclick", `deleteEmployee(${i})`);
        deleteButton[i].innerHTML = "Удалить";
        for (let j = 0; j < 5; j++) {
            tds[j] = document.createElement("td");
            if (j == 0) {
                tds[j].innerHTML = employees[i].department;
            } else if (j == 1) {
                tds[j].innerHTML = employees[i].name;
            } else if (j == 2) {
                tds[j].innerHTML = employees[i].email;
            } else if (j == 3) {
                if (employees[i].admin) {
                    tds[j].innerHTML = "Да";
                } else { tds[j].innerHTML = "Нет"; }
            } else if (j == 4) {
                if (i != 0 && i != tokens.indexOf(userToken)) {
                    tds[j].appendChild(deleteButton[i]);
                }
            }
            rows[i].appendChild(tds[j]);
        }
        table.appendChild(rows[i]);
    }
}

function drawListOfEmployees(divId) {
    let divForList = document.getElementById(divId);
    let bigDiv = document.createElement("div");
    let titles = new Array(numberOfDeparts);
    let uls = new Array(numberOfDeparts);
    let lis = new Array(numberOfEmployees);
    let inputs = new Array(numberOfEmployees);
    for (let i = 0; i < numberOfDeparts; i++) {
        uls[i] = document.createElement("ul");
        uls[i].className = "list-group";
        titles[i] = document.createElement("div");
        titles[i].className = "h5 my-2";
        titles[i].innerHTML = departments[i];
        bigDiv.appendChild(titles[i]);
        let departTerm = departments[i];
        let departFilter = employees.filter(emps => emps.department === departTerm);
        for (let j = 0; j < departFilter.length; j++) {
            lis[j] = document.createElement("li");
            lis[j].className = "list-group-item";
            lis[j].innerHTML = departFilter[j].name;
            inputs[j] = document.createElement("input");
            inputs[j].className = "form-check-input mx-3";
            inputs[j].setAttribute("type", "checkbox");
            inputs[j].setAttribute("name", "checkEmps");
            lis[j].appendChild(inputs[j]);
            uls[i].appendChild(lis[j]);
        }
        bigDiv.appendChild(uls[i]);
    }
    divForList.appendChild(bigDiv);
}

function drawTableOfRoutes(tableBodyId) {
    if(numberOfRoutes != 0) {
        let table = document.getElementById(tableBodyId);
        let rows = new Array(numberOfRoutes);
        let ths = new Array(numberOfRoutes);
        let tds = new Array(numberOfRoutes);
        for (let i = 0; i < numberOfRoutes; i++) {
            rows[i] = document.createElement('tr');
            rows[i].setAttribute("class", "d-flex");
            ths[i] = document.createElement('th');
            ths[i].setAttribute("class", "col-3");
            ths[i].innerHTML = routes[i].name;
            rows[i].appendChild(ths[i]);
            tds[i] = document.createElement('td');
            tds[i].setAttribute("class", "col-9");
            let routeRows = new Array(routes[i].employees.length);
            let routeTds = new Array(3);
            for (let j = 0; j < routes[i].employees.length; j++) {
                routeRows[j] = document.createElement('tr');
                routeRows[j].setAttribute("class", "d-flex")
                for (let k = 0; k < 3; k++) {
                    routeTds[k] = document.createElement('td');
                    routeTds[k].setAttribute("class", "col-4");
                    if (k == 0) {
                        routeTds[k].innerHTML = routes[i].employees[j].department;
                    } else if (k == 1) {
                        routeTds[k].innerHTML = routes[i].employees[j].name;
                    } else if (k == 2) {
                        routeTds[k].innerHTML = routes[i].employees[j].period / 60 + " ч.";
                    }
                    routeRows[j].appendChild(routeTds[k]);
                }
                tds[i].appendChild(routeRows[j]);
            }
            rows[i].appendChild(tds[i]);
//            let rowEmps = new Array(routes[i].employees.length);
//            let tds = new Array(2);
//            for (let j = 0; j < routes[i].employees.length; j++) {
//                rowEmps[j] = document.createElement('tr');
//                rowEmps[j].innerHTML = routes[i].employees[j].name + ", время: 24ч";
//                rows[i].appendChild(rowEmps[j]);
//            }
            table.appendChild(rows[i]);
        }
    } else {
        let divForImage = document.getElementById('nulllRoutesImage');
        let nullRoutesImage = document.createElement('img');
        nullRoutesImage.setAttribute("src", "nullRoutesImage.png");
        nullRoutesImage.setAttribute("class", "h-50 w-50");
        divForImage.appendChild(nullRoutesImage);
    }
}

function addEmployee() {
    let docForm = document.getElementById('formAddEmployee');
    let isAdmin;
    if (docForm.elements.isAdminFlag.checked) {
        isAdmin = true;
    } else {
        isAdmin = false;
    }

    let newEmployee = {
        "department" : docForm.elements.newEmployeeDepartment.value,
        "name" : docForm.elements.newEmployeeName.value,
        "email" : docForm.elements.newEmployeeEmail.value,
        "admin" : isAdmin,
    }
    
    if (isAdmin) {
        newEmployee.token = "a" + numberOfEmployees;
    } else {
        newEmployee.token = "e" + numberOfEmployees;
    }
    
    addDepartment(docForm.elements.newEmployeeDepartment.value);
    
    employees[numberOfEmployees] = newEmployee;
    numberOfEmployees = numberOfEmployees + 1;
    localStorage.setItem("employees", JSON.stringify(employees));
    localStorage.setItem("numberOfEmployees", numberOfEmployees);
    window.location.reload();
}

function addDepartment(dptName) {
    if (!departments.includes(dptName)) {
        departments[numberOfDeparts] = dptName;
        numberOfDeparts = numberOfDeparts + 1;
    }
    localStorage.setItem("departments", JSON.stringify(departments));
    localStorage.setItem("numberOfDeparts", numberOfDeparts);
}

function addRoute() {
    let docForm = document.getElementById('formAddRoute');
    let routeLis = document.getElementsByClassName('list-group-item');
    let routeInputs = document.getElementsByName('checkEmps');

    let routeEmployees = [];
    let k = 0;
    for (let i = 0; i < routeLis.length; i++) {
        if (routeInputs[i].checked) {
            routeEmployees[k] = {
                "name" : routeLis[i].innerText,
                "department" : employees.find(emps => emps.name === routeLis[i].innerText).department,
                "period" : 1440
            }
            k = k + 1;
        }
    }
    
    let rEmpsLen = routeEmployees.length;
    
    routeEmployees.sort(function(a, b) {
        if (a.department < b.department) {
            return -1;
        } else {
            return 0;
        }
    });
    
    let lawyerId, dirId, temp;
    for (let i = 0; i < routeEmployees.length; i++) {
        if (routeEmployees[i].department == "начальник юридического отдела") {
            lawyerId = i;
        } else if (routeEmployees[i].department == "директор") {
            dirId = i;
        }
    }
    temp = routeEmployees[rEmpsLen-2];
    routeEmployees[rEmpsLen-2] = routeEmployees[lawyerId];
    routeEmployees[lawyerId] = temp;
    temp = routeEmployees[rEmpsLen-1];
    routeEmployees[rEmpsLen-1] = routeEmployees[dirId];
    routeEmployees[dirId] = temp;
    
    routeEmployees[rEmpsLen-1].period = 2880;
    
    let newRoute = {
        "name" : docForm.elements.routeName.value,
        "type" : docForm.elements.newRouteContractType.value,
        "employees" : routeEmployees
    }
    
    routes[numberOfRoutes] = newRoute;
    numberOfRoutes = numberOfRoutes + 1;
    localStorage.setItem("routes", JSON.stringify(routes));
    localStorage.setItem("numberOfRoutes", numberOfRoutes);
    window.location.reload();
}

function deleteEmployee(index) {
    let deletedEmpName = employees[index].name;
    let deletedEmpDep = employees[index].department;
    employees = employees.filter(emps => emps.name != deletedEmpName);
    let tempArr = employees.filter(emps => emps.department == deletedEmpDep);
    if (!(Array.isArray(tempArr) && tempArr.length)) {
        departments = departments.filter(dep => dep != deletedEmpDep);
        numberOfDeparts = numberOfDeparts - 1;
    }
    numberOfEmployees = numberOfEmployees - 1;
    localStorage.setItem('employees', JSON.stringify(employees));
    localStorage.setItem('numberOfEmployees', JSON.stringify(numberOfEmployees));
    localStorage.setItem('departments', JSON.stringify(departments));
    localStorage.setItem('numberOfDeparts', JSON.stringify(numberOfDeparts));
    window.location.reload();
}
