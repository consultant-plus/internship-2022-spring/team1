let cardHeader = document.getElementsByClassName("card-header d-flex justify-content-between")[0];
let partOfTitle = document.getElementsByName("approvalProgress")[0];
let docTitle = document.createElement("div");

let conts = JSON.parse(localStorage.getItem('contracts'));
let comments = JSON.parse(localStorage.getItem('comments'));
let contId = JSON.parse(localStorage.getItem('viewingDocId'));
let routes = JSON.parse(localStorage.getItem('routes'));
let approvals = JSON.parse(localStorage.getItem('approvals'));
let userToken = JSON.parse(sessionStorage.getItem("userToken"));
let contsOnApproval = JSON.parse(localStorage.getItem('contsOnApproval'));


let employees = JSON.parse(localStorage.getItem('employees'));
let numberOfEmployees  = JSON.parse(localStorage.getItem("numberOfEmployees"));

console.log(contId);

let routeId = -2;

for (let i = 0; i < routes.length; i++) {
    if (routes[i].type == conts[contId].type) {
        routeId = i;
        break;
    }
}


let approvalId = -3;
for (let i = 0; i < approvals.length; i++) {
    if (approvals[i].contractId == contId) {
        approvalId = i;
        break;
    }
}


let span = document.getElementsByClassName('navbar-text')[0];
span.innerHTML = employees.find(emps => emps.token === userToken).name + ', ' + 
                employees.find(emps => emps.token === userToken).department;
    
drawApprovalProgress('approvalProgressTableBody');

//console.log(routes[routeId].employees);

docTitle.setAttribute("class", "h4");
docTitle.innerHTML = conts[contId].name;
cardHeader.insertBefore(docTitle, partOfTitle);    
  
function drawApprovalProgress(tableBodyId) {
    let table = document.getElementById(tableBodyId);
    let rows = new Array(routes[routeId].employees.length);
    let viewCommentsBtn = new Array(routes[routeId].employees.length);
    let tds = new Array(5);

    let tableViewCommentsBody = document.getElementById('tableViewCommentsBody');
    let actualComment = comments.find(cmmnt => cmmnt.contractId === contId);
    let commentsEmps = actualComment.employees;

    for (let i = 0; i < routes[routeId].employees.length; i++) {
        
        rows[i] = document.createElement('tr');
        viewCommentsBtn[i] = document.createElement('button');
        viewCommentsBtn[i].setAttribute("type", "button");
        viewCommentsBtn[i].setAttribute("class", "btn btn-secondary");
        viewCommentsBtn[i].setAttribute("data-bs-toggle", "modal");
        viewCommentsBtn[i].setAttribute("data-bs-target", "#viewComments");
        viewCommentsBtn[i].innerHTML = "Посмотреть";
        
        let modalCommentsRows = new Array(commentsEmps[i].comment.length);
        for (let k = 0; k < modalCommentsRows.length; k++) {
            modalCommentsRows[k] = document.createElement("tr");
            modalCommentsTds = new Array(3);
            for (let h = 0; h < 3; h++) {
                modalCommentsTds[h] = document.createElement("td");
                if (h == 0) {
                    modalCommentsTds[h].innerHTML = commentsEmps[i].comment[k].point;
                } else if (h == 1) {
                    modalCommentsTds[h].innerHTML = commentsEmps[i].comment[k].prevVersion;
                } else if (h == 2) {
                    modalCommentsTds[h].innerHTML = commentsEmps[i].comment[k].nextVersion;
                }
                modalCommentsRows[k].appendChild(modalCommentsTds[h]);
            }
            tableViewCommentsBody.appendChild(modalCommentsRows[k]);
        }
        
        for (let j = 0; j < 5; j++) {
            tds[j] = document.createElement('td');
            if (j == 0) {
                tds[j].innerHTML = routes[routeId].employees[i].department;
            } else if (j == 1) {
                tds[j].innerHTML = routes[routeId].employees[i].name;
            } else if (j == 2) {
                if (approvals[approvalId].employees[i].status == "sent") {
                    if (approvals[approvalId].employees[i].department == "начальник юридического отдела") {
                        tds[j].innerHTML = "Отправлено на визирование";
                    } else if (approvals[approvalId].employees[i].department == "директор") {
                        tds[j].innerHTML = "Отправлено на утверждение"; 
                    } else {
                        tds[j].innerHTML = "Отправлено на согласование";               
                    }
                } else if (approvals[approvalId].employees[i].status == "approved") {
                    tds[j].innerHTML = "Согласовано";
                } else if (approvals[approvalId].employees[i].status == "approvedWithComments") {
                    tds[j].innerHTML = "Согласовано c замечаниями";
                } else if (approvals[approvalId].employees[i].status == "sighted") {
                    tds[j].innerHTML = "Завизировано";
                } else if (approvals[approvalId].employees[i].status == "confirmed") {
                    tds[j].innerHTML = "Утверждено";
                }
            } else if (j == 3) {
                if (approvals[approvalId].employees[i].status == "sent") {
                    if (approvals[approvalId].employees[i].department == "директор") {
                        tds[j].innerHTML = "48 ч.";
                    } else {
                        tds[j].innerHTML = "24 ч."; 
                    }
                }
            } else if (j == 4) {
                if (approvals[approvalId].employees[i].status == "approved") {
                    tds[j].innerHTML = "Отсутствуют";
                } else if (approvals[approvalId].employees[i].status == "approvedWithComments") {
                    tds[j].appendChild(viewCommentsBtn[i]);
                }
            }
            rows[i].appendChild(tds[j]);
        }
        table.appendChild(rows[i]);
    }
}
let approvalStatuses = new Array(routes[routeId].employees.length);
for (let i = 0; i < routes[routeId].employees.length; i++) {
    approvalStatuses[i] = false;
}

for (let i = 0; i < routes[routeId].employees.length; i++) {
    if (approvals[approvalId].employees[i].status == "approved" || 
        approvals[approvalId].employees[i].status == "approvedWithComments" || 
        approvals[approvalId].employees[i].status == "sighted" ||
        approvals[approvalId].employees[i].status == "confirmed") {
        approvalStatuses[i] = true;
    } else {
        approvalStatuses[i] = false;
        
    }
}

let num = 0;
for (let i = 0; i < routes[routeId].employees.length; i++) {
    if (approvalStatuses[i] == true) {
        num = num + 1;
    }
}
if (conts[contId].status == "approval") {
    let l = approvals[approvalId].employees.length-2;
    if (num >= routes[routeId].employees.length-2) {
        if (num == routes[routeId].employees.length-2) {
            if (approvals[approvalId].employees[l].status != "sent") {
                let divForBtn = document.getElementById('SendToBtn');
                let sendToBtn = document.createElement('button');
                sendToBtn.setAttribute("type", "button");
                sendToBtn.setAttribute("class", "btn btn-success");
                sendToBtn.setAttribute("data-bs-toggle", "modal");
                sendToBtn.setAttribute("data-bs-target", "#sendToLawyer");
                sendToBtn.innerHTML = "Отправить на визирование";
                divForBtn.appendChild(sendToBtn);
            }
        } else if (num == routes[routeId].employees.length-1) {
            l = l+1;
            if (approvals[approvalId].employees[l].status != "sent") {
                let divForBtn = document.getElementById('SendToBtn');
                let sendToBtn = document.createElement('button');
                sendToBtn.setAttribute("type", "button");
                sendToBtn.setAttribute("class", "btn btn-success");
                sendToBtn.setAttribute("onclick", "sendToDir()");
                sendToBtn.innerHTML = "Отправить директору";
                divForBtn.appendChild(sendToBtn);  
            }
        } else if (num == routes[routeId].employees.length) {
            conts[contId].status = "approved";
            contsOnApproval = contsOnApproval - 1;
            localStorage.setItem('contracts', JSON.stringify(conts));
            localStorage.setItem('contsOnApproval', JSON.stringify(contsOnApproval));
        }
}
}


function sendToLawyer() {
    let l = approvals[approvalId].employees.length-2;
    approvals[approvalId].employees[l].status = "sent";
    localStorage.setItem('approvals', JSON.stringify(approvals));
    window.location.reload();
}

function sendToDir() {
    let l = approvals[approvalId].employees.length-1;
    approvals[approvalId].employees[l].status = "sent";
    localStorage.setItem('approvals', JSON.stringify(approvals));
    window.location.reload();
}

localStorage.setItem('viewingDocId', JSON.stringify(contId));