window.onload = () => {    
    drawTable();
}

let numberOfConts = JSON.parse(localStorage.getItem('numberOfContracts'));
let conts = JSON.parse(localStorage.getItem('contracts'));
let employees = JSON.parse(localStorage.getItem("employees"));
let numberOfEmployees  = JSON.parse(localStorage.getItem("numberOfEmployees"));
let userToken = JSON.parse(sessionStorage.getItem("userToken"));
let approvals = JSON.parse(localStorage.getItem("approvals"));
let contsOnApproval = JSON.parse(localStorage.getItem("contsOnApproval"));
let approvalList, apprEmps, sentEmps, currEmpIndex, empsNames;

localStorage.setItem("viewingDocId", -1);
    
let btnGroup = document.getElementsByClassName('btnGroupInTitile')[0];
let span = document.getElementsByClassName('navbar-text')[0];
span.innerHTML = employees.find(emps => emps.token === userToken).name + ', ' + 
                employees.find(emps => emps.token === userToken).department;

let currentEmployee = employees.find(emps => emps.token === userToken);


if (userToken == "a0") {
    let addContBtn = document.createElement('button');
    addContBtn.setAttribute("type", "button");
    addContBtn.setAttribute("class", "btn btn-success me-2");
    addContBtn.setAttribute("data-bs-toggle", "modal");
    addContBtn.setAttribute("data-bs-target", "#addContract");
    addContBtn.setAttribute("name", "addContractButton");
    addContBtn.innerHTML = "Добавить договор";
    btnGroup.appendChild(addContBtn);
}


if (userToken[0] == "a") {
    let controlBtn = document.createElement('button');
    controlBtn.setAttribute("type", "button");
    controlBtn.setAttribute("class", "btn btn-secondary me-2");
    controlBtn.setAttribute("onclick", "window.location.replace(\"/admin_panel.html\")");
    controlBtn.innerHTML = "Управление";
    btnGroup.appendChild(controlBtn);
}


function drawTable() {
    if (numberOfConts != 0 && userToken == "a0") {
        let table = document.getElementById('tableOfContracts');
        let tableHead = document.getElementById('tableOfContractsHead')
        let trh = document.createElement("tr");
        let thH = new Array(4);
        for (let i = 0; i < 4; i++) {
            thH[i] = document.createElement("th");
            if (i == 0) {
                thH[i].innerHTML = "№";
            } else if (i == 1) {
                thH[i].innerHTML = "Договор";
            } else if (i == 2) {
                thH[i].innerHTML = "Статус";
            } else if (i == 3) {
                thH[i].innerHTML = "Срок";
            }
            trh.appendChild(thH[i]);
        }
        tableHead.appendChild(trh);
        let tableBody = document.getElementById('tableOfContractsBody');
        let rows = new Array(numberOfConts);
        let ths = new Array(numberOfConts); 
        let tds = new Array(3);
        for (let i = 0; i < numberOfConts; i++) {
            approvalList = approvals.find(appr => appr.contractId == i);
            if (conts[i].status == "approval") {
                apprEmps = approvalList.employees;
                sentEmps = apprEmps.filter(emps => emps.status == "sent");
            }
            rows[i] = document.createElement("tr");
            rows[i].className = "hide";
            ths[i] = document.createElement("th");
            ths[i].innerHTML = i + 1;
            rows[i].appendChild(ths[i]);
            for (let j = 0; j < 3; j++) {
                tds[j] = document.createElement("td");
                tds[j].className = "pt-3-half";
                if (j == 0) {
                    tds[j].innerHTML = conts[i].name;
                    tds[j].setAttribute("onclick", `viewContract(${i})`);
                } else if (j == 1) {
                    if (conts[i].status == "approval") {
                        let statusStr = "";
                        for (let m = 0; m < sentEmps.length; m++) {
                            statusStr = statusStr + sentEmps[m].department + "; ";
                        }
                        tds[j].innerHTML = "В процессе согласования " + statusStr;
                        tds[j].setAttribute("onclick", `viewApprovalProgress(${i})`);
                    } else if (conts[i].status == "uploaded") {
                        tds[j].innerHTML = "Ожидает отправки на согласование";
                    } else if (conts[i].status == "approved") {
                        tds[j].innerHTML = "Согласовано всеми участниками";
                    }
                } else {
                    if (conts[i].status == "approval") {
                        tds[j].innerHTML = "4 дня";
                    } else {
                        tds[j].innerHTML = "";
                    }
                }
                rows[i].appendChild(tds[j]);
            }
            tableBody.appendChild(rows[i]);
        }
        table.appendChild(tableHead);
        table.appendChild(tableBody);
    } else if (userToken[1] > "0") {
        if (contsOnApproval != 0) {
            let allApprovalEmployees = approvals.map(appr => appr.employees);
            let allApprEmpsNames = [];
            for (let i = 0; i < allApprovalEmployees.length; i++) {
                allApprEmpsNames[i] = allApprovalEmployees[i].map(appr => appr.name);
            }
            
            allApprEmpsNames = filterNames(allApprEmpsNames);
            console.log(allApprEmpsNames);
            
            let approvalIndexes = [];
            for (let i = 0; i < allApprEmpsNames.length; i++) {
                if (allApprEmpsNames[i].indexOf(currentEmployee.name) != -1) {
                    approvalIndexes[i] = i;
                }
            }
            let contIdsOnApproval = [];
            for (let i = 0; i < approvalIndexes.length; i++) {
                contIdsOnApproval[i] = approvals[approvalIndexes[i]].contractId;
            }
            
            
            
            let table = document.getElementById('tableOfContracts');
            let tableHead = document.getElementById('tableOfContractsHead')
            let trh = document.createElement("tr");
            let thH = new Array(4);
            for (let i = 0; i < 4; i++) {
                thH[i] = document.createElement("th");
                if (i == 0) {
                    thH[i].innerHTML = "№";
                } else if (i == 1) {
                    thH[i].innerHTML = "Договор";
                } else if (i == 2) {
                    thH[i].innerHTML = "Статус";
                } else if (i == 3) {
                    thH[i].innerHTML = "Срок";
                }
                trh.appendChild(thH[i]);
            }
            tableHead.appendChild(trh);
            
            let tableBody = document.getElementById('tableOfContractsBody');
            let rows = new Array(contIdsOnApproval.length);
            let ths = new Array(contIdsOnApproval.length); 
            let tds = new Array(3);
            for (let i = 0; i < contIdsOnApproval.length; i++) {
                rows[i] = document.createElement("tr");
                rows[i].className = "hide";
                ths[i] = document.createElement("th");
                ths[i].innerHTML = i + 1;
                rows[i].appendChild(ths[i]);
                for (let j = 0; j < 3; j++) {
                    tds[j] = document.createElement("td");
                    tds[j].className = "pt-3-half";
                    if (j == 0) {
                        tds[j].innerHTML = conts[contIdsOnApproval[i]].name;
                        tds[j].setAttribute("onclick", `viewContract(${i})`);
                    } else if (j == 1) {
                        empsNames = approvals[i].employees.map(emps => emps.name);
                        currEmpIndex = empsNames.indexOf(currentEmployee.name);
                        if (approvals[i].employees[currEmpIndex].status == "sent") {
                            if (approvals[i].employees[currEmpIndex].department == "начальник юридического отдела") {
                                tds[j].innerHTML = "Ожидает визирования";
                            } else if (approvals[i].employees[currEmpIndex].department == "директор") {
                                tds[j].innerHTML = "Ожидает утверждения";
                            } else {
                                tds[j].innerHTML = "Ожидает согласования";
                            }
                        } else if (approvals[i].employees[currEmpIndex].status == "approved" || approvals[i].employees[currEmpIndex].status == "approvedWithComments") {
                            tds[j].innerHTML = "Согласовано";
                        } else if (approvals[i].employees[currEmpIndex].status == "sighted") {
                            tds[j].innerHTML = "Завизировано";
                        } else if (approvals[i].employees[currEmpIndex].status == "confirmed") {
                            tds[j].innerHTML = "Утверждено";
                        }                      
                    } else {
                        if (approvals[i].employees[currEmpIndex].department == "директор") {
                            tds[j].innerHTML = "48 ч.";
                        } else {
                            tds[j].innerHTML = "24 ч.";
                        }
                    }
                    rows[i].appendChild(tds[j]);
                }
                tableBody.appendChild(rows[i]);
            }
            table.appendChild(tableHead);
            table.appendChild(tableBody);
            
            
            
            
            
            
        } else {
            let divForImage = document.getElementById('nullContsImage');
            let img = document.createElement("img");
            img.setAttribute("src", "nullContsImage.png");
            img.setAttribute("class", "h-50 w-50");
            divForImage.appendChild(img);
        }
    } else if (numberOfConts == 0) {
        let divForImage = document.getElementById('nullContsImage');
        let img = document.createElement("img");
        img.setAttribute("src", "nullContsImage.png");
        img.setAttribute("class", "h-50 w-50");
        divForImage.appendChild(img);
    }
}

function addContract(evt) {
    let docForm = document.getElementById('formAddContract');
    let file = docForm.elements.contractFile.files[0];
    let contType = docForm.elements.contractType.value;
    let newContract = {
        number: docForm.elements.contractNumber.value,
        type: contType,
        status: "uploaded"
    }
    
    if (contType == 1) {
        newContract.name = 'Договор аренды ' + newContract.number;
    } else if (contType == 2) {
        newContract.name = 'Договор поставки ' + newContract.number;
    } else {
        newContract.name = 'Договор возмездного оказания услуг ' + newContract.number;
    }
    
    var reader = new FileReader();
    reader.readAsDataURL(file)
    
    reader.onload = function() {
        newContract.file = reader.result;
        conts[numberOfConts] = newContract;
        numberOfConts = numberOfConts + 1;
        localStorage.setItem('contracts', JSON.stringify(conts));  
        localStorage.setItem('numberOfContracts', numberOfConts);
    }
    localStorage.setItem('viewingDocId', numberOfConts);
    window.location.assign("/view_doc.html");
}

function viewContract(contrId) {
    localStorage.setItem("viewingDocId", contrId);
    window.location.replace("/view_doc.html");
}

function viewApprovalProgress(contrId) {
    localStorage.setItem("viewingDocId", contrId);
    window.location.replace("/approval_progress.html");
}

function filterNames(arr) {
    let seen = {};
    let out = [];
    let j = 0;
    for (let i = 0; i < arr.length; i++) {
         let item = arr[i];
         if (seen[item] !== 1) {
               seen[item] = 1;
               out[j++] = item;
         }
    }
    return out;
}