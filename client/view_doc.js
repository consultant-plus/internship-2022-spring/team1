let cardHeader = document.getElementsByClassName("card-header d-flex justify-content-between")[0];
let buttonGroup = document.getElementsByName("ButtonsInHeader")[0];
let docTitle = document.createElement("div");
let cardBody = document.getElementsByClassName("card-body text-center")[0];
let contBody = document.createElement("object");
let userToken = JSON.parse(sessionStorage.getItem("userToken"));
let conts = JSON.parse(localStorage.getItem('contracts'));
let employees = JSON.parse(localStorage.getItem('employees'));
let numberOfEmployees  = JSON.parse(localStorage.getItem("numberOfEmployees"));
let contId = JSON.parse(localStorage.getItem('viewingDocId'));
let routes = JSON.parse(localStorage.getItem('routes'));
let approvals = JSON.parse(localStorage.getItem('approvals'));
let contsOnApproval = JSON.parse(localStorage.getItem('contsOnApproval'));
let comments = JSON.parse(localStorage.getItem('comments'));
let viewingDocId = JSON.parse(localStorage.getItem('viewingDocId'));

let actualEmployee = employees.find(emps => emps.token === userToken);
let actualComments = comments.find(cmmnt => cmmnt.contractId === contId);
let divCard, divCardBody, divCardTitle;

let certainApproval, approvalIndex, certainEmployee, employeeIndex;

if (conts[contId].status == "approval") {
    certainApproval = approvals.find(appr => appr.contractId === viewingDocId);
    approvalIndex = approvals.indexOf(certainApproval);
    certainEmployee = approvals[approvalIndex].employees.find(emps => emps.name === actualEmployee.name);
    employeeIndex = approvals[approvalIndex].employees.indexOf(certainEmployee);
}


let routeId = -2;
for (let i = 0; i < routes.length; i++) {
    if (routes[i].type == conts[contId].type) {
        routeId = i;
        break;
    }
}

docTitle.setAttribute("class", "h4");
docTitle.innerHTML = conts[contId].name;
cardHeader.insertBefore(docTitle, buttonGroup);   

let viewingContDiv = document.getElementById('viewingDocBody');
let divForDoc = document.createElement("div");
divForDoc.setAttribute("class", "w-50");
let contObject = document.createElement("object");
if (conts[contId.type] == 1) {
    contObject.setAttribute("data", "example_rent.pdf");
} else if (conts[contId].type == 2) {
    if (conts[contId].status == "approval") {
        let l = approvals[approvalIndex].employees.length;
        if (approvals[approvalIndex].employees[l-2].status == "sent" || approvals[approvalIndex].employees[l-2].status == "sighted") {
            contObject.setAttribute("data", "example_delivery_2.pdf");
        } else {
            contObject.setAttribute("data", "example_delivery_1.pdf");
        }
    } else {
        contObject.setAttribute("data", "example_delivery_1.pdf");
    }
} else if (conts[contId].type == 3) {
    contObject.setAttribute("data", "example_services.pdf");
}
contObject.setAttribute("type", "application/pdf");
contObject.setAttribute("width", "100%");
contObject.setAttribute("height", "720");
divForDoc.appendChild(contObject);
viewingContDiv.appendChild(divForDoc);

let currentEmployee = employees.find(emps => emps.token === userToken);

let span = document.getElementsByClassName('navbar-text')[0];
span.innerHTML = employees.find(emps => emps.token === userToken).name + ', ' + 
                employees.find(emps => emps.token === userToken).department;


if (conts[contId].status == "uploaded") {
    let approvalButton = document.createElement("button");
    approvalButton.setAttribute("type", "button");
    approvalButton.setAttribute("class", "btn btn-success mx-2");
    approvalButton.setAttribute("onclick", "sendOnApproval()");
    approvalButton.innerHTML = "Отправить на согласование";
    buttonGroup.appendChild(approvalButton);
} else if (conts[contId].status == "approval") {
    if (userToken != "a0") {
            divCard = document.createElement("div");
            divCardBody = document.createElement("div");
            divCardTitle = document.createElement("h5");
            divCardTitle.setAttribute("name", "commentsTitle");
            divCard.setAttribute("class", "card w-50");
            divCardBody.setAttribute("class", "card-body");
            divCardTitle.setAttribute("class", "card-title my-2");
            divCardTitle.innerHTML = "Замечания";
            divCardBody.appendChild(divCardTitle);
            divCard.appendChild(divCardBody);
            viewingContDiv.appendChild(divCard);
    } else {
        divCard = document.createElement("div");
        divCardBody = document.createElement("div");
        divCardTitle = document.createElement("h5");
        divCardTitle.setAttribute("name", "commentsTitle");
        divCard.setAttribute("class", "card w-50");
        divCardBody.setAttribute("class", "card-body");
        divCardTitle.setAttribute("class", "card-title my-2");
        divCardTitle.innerHTML = "Замечания";
        divCardBody.appendChild(divCardTitle);
        divCard.appendChild(divCardBody);
        viewingContDiv.appendChild(divCard);
    }
} else if (conts[contId].status == "approved") {
    divCard = document.createElement("div");
    divCardBody = document.createElement("div");
    divCardTitle = document.createElement("h5");
    divCardTitle.setAttribute("name", "commentsTitle");
    divCard.setAttribute("class", "card w-50");
    divCardBody.setAttribute("class", "card-body");
    divCardTitle.setAttribute("class", "card-title my-2");
    divCardTitle.innerHTML = "Замечания";
    divCardBody.appendChild(divCardTitle);
    divCard.appendChild(divCardBody);
    viewingContDiv.appendChild(divCard);
}

if (userToken == "a0") {
    if (conts[contId].status == "uploaded") {
            divCard = document.createElement("div");
            divCardBody = document.createElement("div");
            divCardTitle = document.createElement("h5");
            divCardTitle.setAttribute("name", "routeTitle");
            divCard.setAttribute("class", "card w-50");
            divCardBody.setAttribute("class", "card-body");
            divCardTitle.setAttribute("class", "card-title my-2");
            divCardTitle.innerHTML = "Маршрут";
            
            let tableRoute = document.createElement("table");
            tableRoute.setAttribute("class", "table");
            let tableRouteHead = document.createElement("thead");
            let trHead = document.createElement("tr");
            let thHead = new Array(3);
            for (let i = 0; i < 3; i++) {
                thHead[i] = document.createElement("th");
                thHead[i].setAttribute("scope", "col");
                if (i == 0) {
                    thHead[i].innerHTML = "Должность";
                } else if (i == 1) {
                    thHead[i].innerHTML = "Сотрудник";
                } else if (i == 2) {
                    thHead[i].innerHTML = "Срок";
                }
                trHead.appendChild(thHead[i]);
            }
            tableRouteHead.appendChild(trHead);
            
            let tableRouteBody = document.createElement("tbody");
            
            let trs = new Array(routes[routeId].employees.length);
            let tds = new Array(3);
            for (let i = 0; i < routes[routeId].employees.length; i++) {
                trs[i] = document.createElement("tr");
                for (let j = 0; j < 3; j++) {
                    tds[j] = document.createElement("td");
                    if (j == 0) {
                        tds[j].innerHTML = routes[routeId].employees[i].department;
                    } else if (j == 1) {
                        tds[j].innerHTML = routes[routeId].employees[i].name;
                    } else if (j == 2) {
                        tds[j].innerHTML = routes[routeId].employees[i].period / 60 + " ч.";
                    }
                    trs[i].appendChild(tds[j]);
                }
                tableRouteBody.appendChild(trs[i]);
            }
            tableRoute.appendChild(tableRouteHead);
            tableRoute.appendChild(tableRouteBody);
            divCardBody.appendChild(tableRoute);
        
        
            divCard.appendChild(divCardTitle);
            divCard.appendChild(divCardBody);
            viewingContDiv.appendChild(divCard);
    } else {
        let routeButton = document.createElement("button");
        routeButton.setAttribute("type", "button");
        routeButton.setAttribute("class", "btn btn-secondary mx-2");
        routeButton.setAttribute("data-bs-toggle", "modal");
        routeButton.setAttribute("data-bs-target", "#Route");
        routeButton.innerHTML = "Маршрут";
        buttonGroup.appendChild(routeButton);
        
//        divCard = document.createElement("div");
//        divCardBody = document.createElement("div");
//        divCardTitle = document.createElement("h5");
//        divCardTitle.setAttribute("name", "routeTitle");
//        divCard.setAttribute("class", "card w-50");
//        divCardBody.setAttribute("class", "card-body");
//        divCardTitle.setAttribute("class", "card-title my-2");
//        divCardTitle.innerHTML = "Ход согласования";
//        
//            
//        let tableRoute = document.createElement("table");
//        tableRoute.setAttribute("class", "table table-bordered table-responsive-md text-center table-hover");
//        let tableRouteHead = document.createElement("thead");
//        let trHead = document.createElement("tr");
//        trHead.setAttribute("class", "d-flex");
//        let thHead = new Array(5);
//        for (let i = 0; i < 5; i++) {
//            thHead[i] = document.createElement("th");
//            if (i == 0) {
//                thHead[i].setAttribute("class", "col-2");
//                thHead[i].innerHTML = "Должность";
//            } else if (i == 1) {
//                thHead[i].setAttribute("class", "col-2");
//                thHead[i].innerHTML = "Ответственный";
//            } else if (i == 2) {
//                thHead[i].setAttribute("class", "col-2");
//                thHead[i].innerHTML = "Статус";
//            } else if (i == 3) {
//                thHead[i].setAttribute("class", "col-1");
//                thHead[i].innerHTML = "Срок";
//            } else if (i == 4) {
//                thHead[i].setAttribute("class", "col-2");
//                thHead[i].innerHTML = "Замечания";
//            }
//            trHead.appendChild(thHead[i]);
//        }
//        tableRouteHead.appendChild(trHead);
//        
//        
    }
} else if (userToken != "a0") {
    if (approvals[approvalIndex].employees[employeeIndex].status == 'sent') {
        if (approvals[approvalIndex].employees[employeeIndex].department == 'начальник юридического отдела') {
            let sightButton = document.createElement("button");
            sightButton.setAttribute("type", "button");
            sightButton.setAttribute("class", "btn btn-success mx-2");
            sightButton.setAttribute("onclick", "sightByLawyer()");
            sightButton.innerHTML = "Завизировать";
            buttonGroup.appendChild(sightButton);
            let divCommentsBody = document.createElement("div");
            divCommentsBody.setAttribute("name", "divCommentsBody");
            let ulsForComments = new Array(actualComments.employees.length);
            for (let i = 0; i < actualComments.employees.length; i++) {
                if (actualComments.employees[i].comment.length > 0) {
                    let lisForComments = new Array(actualComments.employees[i].comment.length);
                    ulsForComments = document.createElement("ul");
                    ulsForComments.setAttribute("class", "list-group list-group-flush my-2");
                    ulsForComments.innerHTML = actualComments.employees[i].name;
                    for (let j = 0; j < actualComments.employees[i].comment.length; j++) {
                        lisForComments[j] = document.createElement("li");
                        lisForComments[j].setAttribute("class", "list-group-item");
                        lisForComments[j].innerHTML = "Пункт: " + actualComments.employees[i].comment[j].point + "<br>" + "Изначальная версия: " + actualComments.employees[i].comment[j].prevVersion + "<br>" + "Изменения: " + actualComments.employees[i].comment[j].nextVersion;
                        ulsForComments.appendChild(lisForComments[j]);
                    }
                    divCommentsBody.appendChild(ulsForComments);
                }
    }
    divCardBody.appendChild(divCommentsBody);
        } else if (approvals[approvalIndex].employees[employeeIndex].department == 'директор') {
            let confirmButton = document.createElement("button");
            confirmButton.setAttribute("type", "button");
            confirmButton.setAttribute("class", "btn btn-success mx-2");
            confirmButton.setAttribute("onclick", "confirmByDir()");
            confirmButton.innerHTML = "Утвердить";
            buttonGroup.appendChild(confirmButton);
            let divCommentsBody = document.createElement("div");
            divCommentsBody.setAttribute("name", "divCommentsBody");
            let ulsForComments = new Array(actualComments.employees.length);
            for (let i = 0; i < actualComments.employees.length; i++) {
                if (actualComments.employees[i].comment.length > 0) {
                    let lisForComments = new Array(actualComments.employees[i].comment.length);
                    ulsForComments = document.createElement("ul");
                    ulsForComments.setAttribute("class", "list-group list-group-flush my-2");
                    ulsForComments.innerHTML = actualComments.employees[i].name;
                    for (let j = 0; j < actualComments.employees[i].comment.length; j++) {
                        lisForComments[j] = document.createElement("li");
                        lisForComments[j].setAttribute("class", "list-group-item");
                        lisForComments[j].innerHTML = "Пункт: " + actualComments.employees[i].comment[j].point + "<br>" + "Изначальная версия: " + actualComments.employees[i].comment[j].prevVersion + "<br>" + "Изменения: " + actualComments.employees[i].comment[j].nextVersion;
                        ulsForComments.appendChild(lisForComments[j]);
                    }
                    divCommentsBody.appendChild(ulsForComments);
                }
            }
        divCardBody.appendChild(divCommentsBody);
        } else {
            let approveButton = document.createElement("button");
            approveButton.setAttribute("type", "button");
            approveButton.setAttribute("class", "btn btn-success mx-2");
            approveButton.setAttribute("onclick", "approveByEmployee()");
            approveButton.innerHTML = "Согласовать";
            buttonGroup.appendChild(approveButton);
//            let approveWithCommentsButton = document.createElement("button");
//            approveWithCommentsButton.setAttribute("type", "button");
//            approveWithCommentsButton.setAttribute("class", "btn btn-warning mx-2");
//            approveWithCommentsButton.setAttribute("onclick", "approveWithComments()");
//            approveWithCommentsButton.innerHTML = "Согласовать с замечаниями";
//            buttonGroup.appendChild(approveWithCommentsButton);
            let divForInputComments = new Array(10);
            for (let j = 0; j < 17; j++) {
                divForInputComments[j] = document.createElement("div");
                divForInputComments[j].setAttribute("class", "input-group row");
                divForInputComments[j].setAttribute("name", "inputCommentGroup");
                let commentsInputs = new Array(3);
                for (let i = 0; i < 3; i++) {
                    commentsInputs[i] = document.createElement("input");
                    commentsInputs[i].setAttribute("type", "text-area");
                    if (i == 0) {
                        commentsInputs[i].setAttribute("class", "col-md-1");  
                    } else {
                        commentsInputs[i].setAttribute("class", "form-control");  
                        commentsInputs[i].setAttribute("height", "100px");
                    }
                    divForInputComments[j].appendChild(commentsInputs[i]);
                }
                divCardBody.appendChild(divForInputComments[j]);
            }
        }

    }
}

if ((conts[contId].status == "approval" || 
     conts[contId].status == "approved") && 
    (userToken == "a0" || 
     approvals[approvalIndex].employees[employeeIndex].status == 'approved' || 
     approvals[approvalIndex].employees[employeeIndex].status == 'approvedWithComments')) {
    let divCommentsBody = document.createElement("div");
    divCommentsBody.setAttribute("name", "divCommentsBody");
    let ulsForComments = new Array(actualComments.employees.length);
    for (let i = 0; i < actualComments.employees.length; i++) {
        if (actualComments.employees[i].comment.length > 0) {
            let lisForComments = new Array(actualComments.employees[i].comment.length);
            ulsForComments = document.createElement("ul");
            ulsForComments.setAttribute("class", "list-group list-group-flush my-2");
            ulsForComments.innerHTML = actualComments.employees[i].name;
            for (let j = 0; j < actualComments.employees[i].comment.length; j++) {
                lisForComments[j] = document.createElement("li");
                lisForComments[j].setAttribute("class", "list-group-item");
                lisForComments[j].innerHTML = "Пункт: " + actualComments.employees[i].comment[j].point + "<br>" + "Изначальная версия: " + actualComments.employees[i].comment[j].prevVersion + "<br>" + "Изменения: " + actualComments.employees[i].comment[j].nextVersion;
                ulsForComments.appendChild(lisForComments[j]);
            }
            divCommentsBody.appendChild(ulsForComments);
        }
    }
    divCardBody.appendChild(divCommentsBody);
}

let routeTableBody = document.getElementById('modalRouteTableBody');
let trs = new Array(routes[routeId].employees.length);
let tds = new Array(3);
for (let i = 0; i < routes[routeId].employees.length; i++) {
    trs[i] = document.createElement("tr");
    for (let j = 0; j < 3; j++) {
        tds[j] = document.createElement("td");
        if (j == 0) {
            tds[j].innerHTML = routes[routeId].employees[i].department;
        } else if (j == 1) {
            tds[j].innerHTML = routes[routeId].employees[i].name;
        } else if (j == 2) {
            tds[j].innerHTML = routes[routeId].employees[i].period / 60 + " ч.";
        }
        trs[i].appendChild(tds[j]);
    }
    routeTableBody.appendChild(trs[i]);
}


function sendOnApproval() {
    if (routeId == -2) {
        alert("Для этого договора отсутствует маршрут!");
        return;
    }
    newApproval = {
        "contractId" : contId,
        "employees" : []
    }
    newComment = {
        "contractId" : contId,
        "employees" : []
    }
    for (let i = 0; i < routes[routeId].employees.length; i++) {
        newApproval.employees[i] = {
            "name" : routes[routeId].employees[i].name,
            "department" : routes[routeId].employees[i].department,
            "period" : routes[routeId].employees[i].period,
            "status" : "sent"
        }
        if (i == routes[routeId].employees.length - 2 || i == routes[routeId].employees.length - 1) {
            newApproval.employees[i].status = "waiting";
        }
        newComment.employees[i] = {
            "name" : routes[routeId].employees[i].name,
            "department" : routes[routeId].employees[i].department,
            "comment" : ""
        }
    }
    let l = comments.length;
    comments[l] = newComment;
    approvals[contsOnApproval] = newApproval;
    contsOnApproval = contsOnApproval + 1;
    conts[contId].status = "approval";
    localStorage.setItem('approvals', JSON.stringify(approvals));
    localStorage.setItem('comments', JSON.stringify(comments));
    localStorage.setItem('contsOnApproval', JSON.stringify(contsOnApproval));
    localStorage.setItem('contracts', JSON.stringify(conts));
    window.location.replace("/approval_progress.html");
}

function approveByEmployee() {
    let inputCommentsArray = document.getElementsByName('inputCommentGroup');
    let inputComs = [];
    for (let i = 0; i < inputCommentsArray.length; i++) {
        inputComs[i] = inputCommentsArray[i].children;
    }
    inputComs = inputComs.filter(inp => (inp[0].value != "") && (inp[1].value != "") && (inp[2].value != ""));
    if (inputComs.length != 0) {
        let newComment = [];
        console.log(inputComs.length);
        for (let i = 0; i < inputComs.length; i++) {
            newComment[i] = {
                "point" : inputComs[i][0].value,
                "prevVersion" : inputComs[i][1].value,
                "nextVersion" : inputComs[i][2].value
            }
        }
        approvals[approvalIndex].employees[employeeIndex].status = 'approvedWithComments';
        comments[approvalIndex].employees[employeeIndex].comment = newComment;
    } else {
        approvals[approvalIndex].employees[employeeIndex].status = 'approved'; 
    }

    localStorage.setItem('approvals', JSON.stringify(approvals));
    localStorage.setItem('comments', JSON.stringify(comments));
    localStorage.setItem('viewingDocId', contId);
    window.location.replace("/contract_list.html");
}

function approveWithComments() {    
    let inputCommentsArray = document.getElementsByName('inputCommentGroup');
    let inputComs = [];
    for (let i = 0; i < inputCommentsArray.length; i++) {
        inputComs[i] = inputCommentsArray[i].children;
    }
    inputComs = inputComs.filter(inp => (inp[0].value != "") && (inp[1].value != "") && (inp[2].value != ""));
    
    let newComment = [];

    console.log(inputComs.length);
    for (let i = 0; i < inputComs.length; i++) {
        newComment[i] = {
            "point" : inputComs[i][0].value,
            "prevVersion" : inputComs[i][1].value,
            "nextVersion" : inputComs[i][2].value
        }
    }

    approvals[approvalIndex].employees[employeeIndex].status = 'approvedWithComments';
    comments[approvalIndex].employees[employeeIndex].comment = newComment;
    localStorage.setItem('approvals', JSON.stringify(approvals));
    localStorage.setItem('comments', JSON.stringify(comments));
    localStorage.setItem('viewingDocId', contId);
    window.location.replace("/contract_list.html");
}

function sightByLawyer() {
    approvals[approvalIndex].employees[employeeIndex].status = 'sighted';
    localStorage.setItem('approvals', JSON.stringify(approvals));
    localStorage.setItem('comments', JSON.stringify(comments));
    localStorage.setItem('viewingDocId', contId);
    window.location.replace("/contract_list.html");
}

function confirmByDir() {
    approvals[approvalIndex].employees[employeeIndex].status = 'confirmed';
    localStorage.setItem('approvals', JSON.stringify(approvals));
    localStorage.setItem('comments', JSON.stringify(comments));
    localStorage.setItem('viewingDocId', contId);
    window.location.replace("/contract_list.html");
}

localStorage.setItem('viewingDocId', contId);
