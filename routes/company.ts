export {};
const express = require('express');
const router = express.Router();
const companyController = require('../database/company.controller');

router.post('/', companyController.createCompany);

module.exports = router;