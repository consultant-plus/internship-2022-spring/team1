const db = require('./db');

class CompanyController{
	async createCompany(req, res){
		const {name, email, INN} = req.body;
		const newCompany = await db.query('INSERT INTO companies (company_name, company_email, company_inn, company_token) values ($1, $2, $3, $4) RETURNING *', [name, email, INN, 'tokentemp']);
		res.send("all ok");
	}
}

module.exports = new CompanyController();